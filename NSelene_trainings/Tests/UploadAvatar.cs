﻿using NSelene_trainings.Core;
using NSelene_trainings.Pages;
using NUnit.Framework;

namespace NSelene_trainings.Tests
{
    [TestFixture]
    class UploadAvatar : BaseTest
    {
        private ProfileBar profileBar = new ProfileBar();

        public void CanUploadAvatar()
        {
            new AuthorizationModal().LoginAsDev();
            new Header().EnterAccount();

            profileBar
                .ClickAddAvatarIcon()
                .UploadImage()
                .ClickOnSaveButton();
            new ModalContainer().ClosePopUp();

            //profileBar.accountPersonImage2.IsImage();
        }
    }
}
