﻿using NSelene_trainings.Core;
using NUnit.Framework;
using NSelene;

using static NSelene.Selene;
using NSelene_trainings.Pages;
using NSelene_trainings.Data;

namespace NSelene_trainings.Tests
{
    [TestFixture]
    class UploadModel : BaseTest
    {
        private UploadModelForm uploadModel = new UploadModelForm();
        private ModalContainer closeModalWindow = new ModalContainer();

        [Test]
        public void CanUploadModel()
        {
            new AuthorizationModal().LoginAsDev();
            //new Header().enterAccount();
            Open(BASE_URL + "/account");

            uploadModel.EnterTittleOfModel(UserData.nameOfModel);

            uploadModel.UploadImages();
            //closeModalWindow.ClosePopUp();

            uploadModel.UploadArchiveWithModel();
            //closeModalWindow.ClosePopUp();

            uploadModel
                    //.EnterModelDescription(UserData.modelDesc)
                    .ChooseGeometryOption()
                    .EnterTags()
                    .EnterTags()
                    .EnterTags()
                    .SpecifyAmountOfPolygons(UserData.polygonsAmount)
                    .ChooseUnwrappedUvs();

            /* Отмечает все radio-кнопки последней опцией в поле */
            uploadModel.MarkAllRadioButtons();
            

            uploadModel
                    .CheckRender()
                    .EnterVertices()
                    .ChooseFormat()
                    .SelectCategory()
                    .CheckCopyrights();
        S("[class='form__check _mb-10 ant-checkbox-wrapper'] [data-__meta]").Click();

            uploadModel.ClickOnUploadModelButton();

            /* Нужно ждать для загрузки модели на сервер. Но это долго...*/
            //new Product().modelTitle.waitUntil(Condition.text(UserData.nameOfModel), 60000);
        }
    }
}
