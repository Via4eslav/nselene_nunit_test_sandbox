﻿using NSelene;
using NSelene_trainings.Core;
using NSelene_trainings.Data;
using NSelene_trainings.Pages;
using NSelene_trainings.Pages.Elements;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace NSelene_trainings.Tests
{
    [TestFixture]
    class SubscribeForNewsletters : BaseTest
    {
        private Footer footer = new Footer();
        private Validation validation = new Validation();

        [Test]
        public void CansSubscribe()
        {
            footer.EnterSubscriberEmail(UserData.fakeEmail);
            footer.MarkCheckbox();
            footer.SubmitSubscribeForm();

            validation.thanksModal
                    .Should(Be.Visible)
                    .Should(Have.Text("Спасибо за подписку"));
            new ModalContainer().ClosePopUp();
        }
    }
}
