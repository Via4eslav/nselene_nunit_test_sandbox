﻿using NSelene_trainings.Pages;
using NUnit.Framework;
using NSelene_trainings.Data;
using NSelene_trainings.Core;
using NSelene;

namespace NSelene_trainings.Tests
{
    [TestFixture]
    class Authorization : BaseTest
    {
        private AuthorizationModal loginModal = new AuthorizationModal();
        private Validation validation = new Validation();
        private Header header = new Header();
        private ModalContainer closeWindow = new ModalContainer();

        [Test]
        public void ExistingUserCanLogin()
        {
            header.OpenLoginModal();

            loginModal
                    .EnterEmail(UserData.qaLogin)
                    .EnterPassword(UserData.userPassword);
            loginModal.ClickLoginButton();

            header.loginButtonAuthorized.Should(Be.Visible);

            header.EnterAccount();
            new NavigationBar().UserCanLogOut();

            header.loginButtonAuthorized.ShouldNot(Have.Text(UserData.qaLogin));
        }
    }
}