﻿using NUnit.Framework;
using NSelene_trainings.Core;

using static NSelene.Selene;
using NSelene_trainings.Pages;
using NSelene;

namespace NSelene_trainings.Tests
{
    [TestFixture]
    public class AskQuestion : BaseTest
    {
        private AskQuestionModal askQuestion = new AskQuestionModal();
        private Header header = new Header();

        [Test]
        public void NewUserCanAskAQuestion()
        {
            header.OpenQuestionModal();

            askQuestion.FormIsAvailable();
            askQuestion
                    .EnterName("test user")
                    .EnterEmail("test@user.com")
                    .ChooseQuestionCategory()
                    .EnterMessage("Hi. I just want to let You know that this form is still working!")
                    .MarkCheckBox()
                    .SubmitForm();

            askQuestion.AssertThatFormIsSubmitted();
        }
        
        [Test]
        public void AuthUserCanAskAQusetion()
        {
            new AuthorizationModal().LoginAsDev();
            header.OpenQuestionModal();

            askQuestion
                    .ChooseQuestionCategory()
                    .EnterMessage("Hi. I just want to let You know that this form is still working!")
                    .MarkCheckBox()
                    .SubmitForm();

        askQuestion.AssertThatFormIsSubmitted();
        }
    }
}