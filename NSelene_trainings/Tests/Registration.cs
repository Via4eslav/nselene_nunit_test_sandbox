﻿
using NUnit.Framework;
using NSelene_trainings.Core;
using System.Threading;
using NSelene_trainings.Data;

using NSelene;
using static NSelene.Selene;
using NSelene_trainings.Pages;

namespace NSelene_trainings.Tests
{
    [TestFixture]
    class Registration : BaseTest
    {
        private Header header = new Header();
        private AuthorizationModal loginModal = new AuthorizationModal();

        [Test]
        public void UserCanRegister()
        {
            header.OpenLoginModal();
            loginModal
                    .SwitchToRegistrationTab()
                    .EnterRegistrationEmail(UserData.fakeEmail)
                    .EnterNickname(UserData.userLogin)
                    .EnterPasswordReg(UserData.testPassword)
                    .ConfirmPassword(UserData.testPassword)
                    .AgreeWithTerms();
            loginModal.ConfirmRegistration();

            Thread.Sleep(5000);
            new ProfileBar().userTitle.Should(Have.Text(UserData.userLogin));

            new NavigationBar().UserCanLogOut();
            header.loginButton.ShouldNot(Have.Text(UserData.userLogin));
        }
    }
}