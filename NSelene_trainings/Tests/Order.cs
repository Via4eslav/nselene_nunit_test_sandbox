﻿using NUnit.Framework;
using NSelene_trainings.Core;
using System.Threading;

using NSelene;
using static NSelene.Selene;

using NSelene_trainings.Pages;

namespace NSelene_trainings.Tests
{
    [TestFixture]
    class Order : BaseTest
    {
        private Header header = new Header();
        private ProductCard productCard = new ProductCard();
        private OrderPage orderPage = new OrderPage();
        private Validation validation = new Validation();

        [Test]
        public void AuthorizedUserCanOrderModel()
        {
            new AuthorizationModal().LoginAsDev();
            Open(BASE_URL + "/all-models");

            OrderAction();
            orderPage.ClickBuyButton();

            validation.thanksModal.Should(Be.Visible);
            Thread.Sleep(3000);
            new Uploads().AssertUserIsAtUploadsPage();
        }

        private void OrderAction()
        {
            productCard.AddToCart();
            //Thread.Sleep(5000);
            header.OpenCartModal();
            new CartModal().ClickOrderButton();
            orderPage.orderTittle.Should(Be.Visible);
        }
    }
}