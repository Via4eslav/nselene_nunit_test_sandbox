﻿using NSelene;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;
using static NSelene.Selene;

namespace NSelene_trainings.Core
{
    [TestFixture]
    public class BaseTest
    {
        protected string BASE_URL = "http://3dbaza.wezom.agency";

        [SetUp]
        public void SetUp()
        {
            SetWebDriver(new ChromeDriver(@"C:\Projects\Drivers"));
            Configuration.Timeout = 20;
            GetWebDriver().Manage().Window.Maximize();

            Open(BASE_URL);
            Thread.Sleep(3000);
        }

        [TearDown]
        public void TearDownDriver()
        {
            GetWebDriver().Quit();
        }
    }
}