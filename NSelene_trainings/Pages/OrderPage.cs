﻿using NSelene;
using System;

using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    public class OrderPage
    {
        public SeleneElement
            buyBtn = S(".undefined .button"),
            orderTittle = S("h1");

        public void ClickBuyButton()
        {
            buyBtn.Click();
        }
    }
}