﻿using NSelene;

using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    public class ProductCard
    {
        public SeleneCollection AddToCartButton()
        {
            return SS(".gcell .catalog-card__basket");
        }

        public void AddToCart()
        {
            AddToCartButton().Get(7).Click();
        }
    }
}