﻿using NSelene;
using OpenQA.Selenium;
using System.Threading;

using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    class ProfileBar
    {
        public SeleneElement
            accountPersonImage = S(".account-person__image [src]"),
            accountPersonImage2 = S(By.XPath(".//div[@class='account']//img[@alt='']")),
            avatarIcon = S(".account-person__action"),
            uploadAvatarDragZone = S("[type='file']"),
            uploadAvatarButton = S("[class='_flex _justify-center _mt-20'] .button"),

            userTitle = S(".account-person__name");
        
        public ProfileBar ClickAddAvatarIcon()
        {
            avatarIcon.Click();
            return this;
        }

        public ProfileBar UploadImage()
        {
            uploadAvatarDragZone.SendKeys(@"C:\Automation C#\nselene_nunit_test_sandbox\NSelene_trainings\resources\nobugs-log.png");
            return this;
        }

        public void ClickOnSaveButton()
        {
            uploadAvatarButton.Click();
            Thread.Sleep(5000);
        }
    }
}