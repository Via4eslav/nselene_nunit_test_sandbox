﻿using NSelene;

using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    public class Uploads
    {
        public SeleneElement uploadsBar = S(".ant-tabs-nav-animated");

        public void AssertUserIsAtUploadsPage()
        {
            uploadsBar.Should(Be.Visible);
        }
    }
}