﻿using NSelene;
using OpenQA.Selenium;

using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    class NavigationBar
    {
        public SeleneElement logOutButton = S(".account-person-sidebar-item:nth-of-type(9) a");
        public void UserCanLogOut()
        {
            logOutButton.Click();
        }
    }
}