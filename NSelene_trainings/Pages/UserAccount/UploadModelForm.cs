﻿using NSelene;
using System;
using OpenQA.Selenium;
using System.Threading;

using static NSelene.Selene;
using System.IO;
using System.Reflection;

namespace NSelene_trainings.Pages
{
    public class UploadModelForm
    {
        public SeleneElement
            nameOfModel = S(By.XPath("//input[@id='title']")),

            addImagesButton = S("#images .button"),
            //filesDropZone = S("[type='file']"),
            filesDropZone = S(By.CssSelector("[type='file']")),

            uploadedFileIcon = S(".anticon-paper-clip"),

            uploadArchiveButton = S("#archive .button"),
            modelDescriptionArea = S("#description"),

            geometrySelect = S("#geometry"),
            geometryOption = S("body [tabindex] [role='option']:nth-of-type(2)"),

            polygonsInput = S("#polygonsCount"),
            verticesInput = S("#acmeCount"),

            texturesRadioYes = S("#texture"),

            uvMapSelect = S("#unwrappedUVs"), //S("#unwrappedUVs .ant-select-selection__rendered"),

            maya = S(".account-center .account-section:nth-of-type(14) .account-section__sub:nth-of-type(1) [value='2']"),
            obj = S(".account-center .account-section__sub:nth-of-type(1) .gcell:nth-of-type(4) [value]"),
            fbx = S(".account-center .gcell:nth-of-type(5) [value='3']"),

            copyrightsCheckbox = S("#creatorIsOwner"),

            uploadModelButton = S("[class='gcell--20'] .button"),

            render = S(".account-center .account-section:nth-of-type(18) .account-section__sub:nth-of-type(1) .gcell:nth-of-type(1) .ant-checkbox-wrapper > span:nth-of-type(2)"),
            renderType = S("[value='21']"),
            format = S(".account-center .account-section:nth-of-type(17) .gcell:nth-of-type(1) .ant-checkbox-wrapper > span:nth-of-type(2)"),
            subFormat = S(".account-center .account-section__sub:nth-of-type(2) .gcell:nth-of-type(1) .ant-checkbox-wrapper > span:nth-of-type(2)"),

            categoryDropDown = S("#mainCategoryId"),
            style = S("[value='4565']"),

            tags = S(".account-center .ant-select-search__field"),
            modalca = S(".ant-modal");

        public SeleneCollection Specs = SS(".ant-radio");
        //{
        //    return SS(".ant-radio");
        //}

        public SeleneElement Specs2 = S(".ant-radio");

        public void MarkAllRadioButtons()
        {
            for (int i = 0; i < Specs.Count; i++)
            {
                Specs.Get(i).Click();
            }
        }

        public UploadModelForm ChooseFormat()
        {
            format.Click();
            subFormat.Click();
            return this;
        }

        public SeleneCollection categoryOptions = SS(".ant-select-dropdown-menu-item");

        public UploadModelForm SelectCategory()
        {
            categoryDropDown.Click();
#pragma warning disable CS0618 // Type or member is obsolete
            categoryOptions.Get(4);
#pragma warning restore CS0618 // Type or member is obsolete
            return this;
        }

        public void EnterTittleOfModel(string modelName)
        {
            Thread.Sleep(3000);
            nameOfModel.SetValue(modelName);
        }

        /*ToDo: Зугрузка нескольких изображений*/
        public void UploadImages()
        {
            addImagesButton.Click();

            GetWebDriver().SwitchTo().Frame(modalca);

            //string path = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), ".", "card-13.png");

            //string folder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //string filePath = Path.Combine(folder, "card-13.png");

            filesDropZone.SendKeys("C:/Automation C#/nselene_nunit_test_sandbox/NSelene_trainings/bin/Debug/netcoreapp2.0/resources/card-13.png");
            Thread.Sleep(8000);
        }

        public static string
                img = @".\resources\card-13.png";               

        public void UploadArchiveWithModel()
        {
            uploadArchiveButton.Click();
            filesDropZone.SendKeys(@".\resources\Desktop.zip");
            Thread.Sleep(10000);
        }

        public UploadModelForm EnterModelDescription(string modelDescription)
        {
            modelDescriptionArea.SetValue(modelDescription);
            return this;
        }

        public UploadModelForm EnterTags()
        {
            tags.SetValue("testTag_" + new Random().Next(1, 100)).PressEscape();
            return this;
        }
            public UploadModelForm ChooseGeometryOption()
        {
            geometrySelect.Click();
            geometryOption.Should(Be.Visible).Click();
            return this;
        }

        public UploadModelForm SpecifyAmountOfPolygons(string polygonsAmount)
        {
            polygonsInput.SetValue(polygonsAmount);
            return this;
        }

        public void ChooseUnwrappedUvs()
        {
            uvMapSelect.Click();
        }

        public UploadModelForm CheckRender()
        {
            render.Click();
            renderType.Click();
            return this;
        }

        public UploadModelForm EnterVertices()
        {
            verticesInput.SetValue("20000");
            return this;
        }

        public void CheckCopyrights()
        {
            copyrightsCheckbox.Click();
        }

        public void ClickOnUploadModelButton()
        {
            uploadModelButton.Click();
        }
    }
}