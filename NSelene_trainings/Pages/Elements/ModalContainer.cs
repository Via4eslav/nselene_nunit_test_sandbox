﻿using NSelene;
using System.Threading;

using static NSelene.Selene;



namespace NSelene_trainings.Pages
{
    class ModalContainer
    {
        public void ClosePopUp()
        {
            Thread.Sleep(2000);
            S(".modal__close").Click();
        }
    }
}