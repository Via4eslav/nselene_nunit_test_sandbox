﻿using NSelene;
using OpenQA.Selenium;

using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    public class Header
    {
        public NSelene.SeleneElement
            headerLogo = S(".header .logo [src]"),
            footerLogo = S(".footer-logo [src]"),

            loginButton = S(".button--noupper"),
            askQuestionButton = S("._flex-noshrink._md-show .button"),
            loginButtonAuthorized = S(".link"),
            searchInput = S("[class='gcell gcell--20 _flex-grow _xs-show'] .ant-select-search__field"),
            searchButton = S(".button--icon"),
            cartIcon = S("");

        public void EnterAccount()
        {
            loginButtonAuthorized.Click();
            S("h1").Should(Be.Visible);
        }

        public SeleneElement GetHeaderCartItems()
        {
            return S(".cart-block__button");
        }

        public void OpenQuestionModal()
        {
            askQuestionButton.Click();
        }

        public void OpenCartModal()
        {
            GetHeaderCartItems().Should(Be.Visible);
            GetHeaderCartItems().Click();
        }

        public void ScrollToHeader()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)GetWebDriver();
            js.ExecuteScript("arguments[0].scrollIntoView(true);", headerLogo);

        }

        public void ScrollToFooter()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)GetWebDriver();
            js.ExecuteScript("arguments[0].scrollIntoView(true);", footerLogo);

        }

        public void OpenLoginModal()
        {
            loginButton.Click();
            new AuthorizationModal().googleCaptcha.Should(Be.Visible);
        }
    }
}