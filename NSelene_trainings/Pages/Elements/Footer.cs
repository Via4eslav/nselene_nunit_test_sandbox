﻿using NSelene;
using System;
using System.Collections.Generic;
using System.Text;

using static NSelene.Selene;

namespace NSelene_trainings.Pages.Elements
{
    class Footer
    {
        public SeleneElement
            subscribeEmail = S("#subscribe-email"),
            subscribeCheckBox = S(".ant-checkbox"),
            subscribeSubmitButton = S(".button.button--white.button--width");

        public SeleneElement footerLogo = S(".footer-logo [src]");

        public void EnterSubscriberEmail(String email)
        {
            subscribeEmail.SetValue(email);
        }

        public void MarkCheckbox()
        {
            subscribeCheckBox.Click();
        }

        public void ClearSubscribeInput()
        {
            subscribeEmail.Clear();
        }

        public void SubmitSubscribeForm()
        {
            subscribeSubmitButton.Click();
        }
    }
}
