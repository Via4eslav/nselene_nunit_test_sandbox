﻿using NSelene;

using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    public class CartModal
    {
        public SeleneElement
            OrderBtn = S(".cart-block__buttons [match]");

        public void ClickOrderButton()
        {
            OrderBtn.Click();
        }
    }
}