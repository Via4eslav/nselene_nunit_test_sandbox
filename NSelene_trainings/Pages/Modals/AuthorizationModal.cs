﻿using NSelene;
using System;
using System.Threading;
using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    public class AuthorizationModal
    {
        public SeleneElement
            userLoginInput = S("#auth-email"),
            userPasswordInput = S("#auth-password"),
            submitButton = S(".modal__container [class='gcell gcell--20']:nth-of-type(6) .button");

        public SeleneElement
            regTab = S(".modal__container [class='gcell gcell--20']:nth-of-type(9) .button"),
            regEmailInput = S("#register-email"),
            nicknameInput = S("#register-name"),
            registerPasswordInput =S("#register-password"),
            confirmPasswordInput = S("#register-password2"),
            termsCheckbox = S(".modal__container .ant-checkbox-wrapper"),
            registrationButton = S(".undefined .button"),
            googleCaptcha = S(".modal__container [class='gcell gcell--20']:nth-of-type(4) [data-__meta]");

        //Registration Actions
        public AuthorizationModal SwitchToRegistrationTab()
        {
            regTab.Click();
            googleCaptcha.Should(Be.Visible);
            return new AuthorizationModal();
        }

        public AuthorizationModal EnterRegistrationEmail(String email)
        {
            regEmailInput.SetValue(email);
            return this;
        }

        public AuthorizationModal EnterNickname(String nickname)
        {
            nicknameInput.SetValue(nickname);
            return this;
        }

        public AuthorizationModal EnterPasswordReg(String userPassword)
        {
            registerPasswordInput.SetValue(userPassword);
            return this;
        }
        
        public AuthorizationModal ConfirmPassword(String password)
        {
            confirmPasswordInput.SetValue(password);
            return this;
        }
        
        public void AgreeWithTerms()
        {
            termsCheckbox.Click();
        }
        
        public void ConfirmRegistration()
        {
            registrationButton.Click();
        }
        //End Registration Actions

        public void LoginAsDev()
        {
            new Header().loginButton.Click();
            EnterEmail("testusername");
            EnterPassword("000000q!");
            ClickLoginButton();
            Thread.Sleep(3000);
            new Header().loginButtonAuthorized.Should(Be.Visible);
        }

        public AuthorizationModal EnterEmail(string devLogin)
        {
            userLoginInput.SetValue(devLogin);
            return this;
        }

        public AuthorizationModal EnterPassword(string devPassword)
        {
            userPasswordInput.SetValue(devPassword);
            Thread.Sleep(3000);
            return this;
        }

        public void ClickLoginButton()
        {
            submitButton.Click();
        }
    }
}