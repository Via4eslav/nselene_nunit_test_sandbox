﻿
using NSelene;
using System;
using static NSelene.Selene;

namespace NSelene_trainings.Pages
{
    class AskQuestionModal
    {
        public NSelene.SeleneElement
            questionModal = S(".ant-modal"),
            nameInput = S("#name"),
            emailInput = S("#email"),
            categoryDropdownMenu = S("#type [role]"), //#type [role]
            questionCategoryOption = S("[tabindex] [role='option']:nth-of-type(1)"),
            textArea = S("#text"),
            termsCheckbox = S(".modal__container .ant-checkbox-wrapper > span:nth-of-type(2)"),
            submitButton = S(".undefined .button");

        public AskQuestionModal EnterName(String userName)
        {
            nameInput.SendKeys(@"");
            nameInput.SetValue(userName);
            return this;
        }

        public AskQuestionModal EnterEmail(String userLogin)
        {
            emailInput.SetValue(userLogin);
            return this;
        }

        public AskQuestionModal ChooseQuestionCategory()
        {
            categoryDropdownMenu.Click();
            questionCategoryOption.Click();
            return this;
        }

        public AskQuestionModal EnterMessage(String userMessage)
        {
            textArea.SetValue(userMessage);
            return this;
        }

        public AskQuestionModal MarkCheckBox()
        {
            termsCheckbox.Click();
            return this;
        }

        public void SubmitForm()
        {
            submitButton.Click();
        }

        public void AssertThatFormIsSubmitted()
        {
            questionModal.ShouldNot(Be.Visible);
        }

        public void FormIsAvailable()
        {
            questionModal.Should(Be.Visible);
        }
    }
}